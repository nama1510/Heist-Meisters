extends "res://Scripts/Character.gd"


var torchOn = true
var motion = Vector2()

var vision_mode_on_cooldown = false
enum vision_mode {DARK, NIGHTVISION}

func _ready():
	Global.Player = self
	vision_mode = DARK

func _process(delta):
	update_motion(delta)

	move_and_slide(motion)


func update_motion(delta):
	
	look_at(get_global_mouse_position())
	
	if Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down") == false:
		motion.y = clamp((motion.y - MAX_SPEED), -MAX_SPEED, 0)
	if Input.is_action_pressed("ui_down") && Input.is_action_pressed("ui_up") == false:
		motion.y = clamp((motion.y + MAX_SPEED), 0, MAX_SPEED)
	else:
		motion.y = lerp(motion.y, 0, FRICTION)
	
	if Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right") == false:
		motion.x = clamp((motion.x - MAX_SPEED), -MAX_SPEED, 0)
	if Input.is_action_pressed("ui_right") && Input.is_action_pressed("ui_left") == false:
		motion.x = clamp((motion.x + MAX_SPEED), 0, MAX_SPEED)
	else:
		motion.x = lerp(motion.x, 0, FRICTION)
		

func _input(event):
	if Input.is_action_just_pressed("ui_vision_mode_changed") and not vision_mode_on_cooldown:
		cycle_vision_mode()
		vision_mode_on_cooldown = true
		$VisionModeTimer.start()

func cycle_vision_mode():
	if vision_mode == DARK:
		get_tree().call_group("interface", "nightVision_mode")
		get_tree().call_group("NPC", "nightVision_mode")
		vision_mode = NIGHTVISION
	elif vision_mode == NIGHTVISION:
		get_tree().call_group("interface", "darkVision_mode")
		get_tree().call_group("NPC", "darkVision_mode")
		vision_mode = DARK



func _on_VisionModeTimer_timeout():
	vision_mode_on_cooldown = false
	


#func toggle_torch():
#	if torchOn == true:
#		$Torch.hide()
#	else:
#		$Torch.show()
#	torchOn = !torchOn
#
#func _input(event):
#	if Input.is_action_just_pressed("ui_select") == true:
#		toggle_torch()


