extends Node

var Player
var navigation
var destinations


#Asset Links - MUST BE CHANGED MANUALLY

var nightvision_on_sfx = "res://SFX/nightvision.wav"
var nightvision_off_sfx = "res://SFX/nightvision_off.wav"

var red_light = "res://GFX/Interface/PNG/dotRed.png"
var green_light = "res://GFX/Interface/PNG/dotGreen.png"

#global audio variables
var button_pushedSFX = "res://SFX/twoTone1.ogg"
var combination_correctSFX = "res://SFX/threeTone1.ogg"

