extends CanvasModulate

const DARK = Color("071f2a")
const NIGHT_VISION = Color("2bc92b")

func _ready():
	add_to_group("interface")
	color = DARK


func nightVision_mode():
	$AudioStreamPlayer.stream = load(Global.nightvision_on_sfx)
	color = NIGHT_VISION
	play_sfx()
	
func darkVision_mode():
	$AudioStreamPlayer.stream = load(Global.nightvision_off_sfx)
	color = DARK
	play_sfx()

func play_sfx():
	$AudioStreamPlayer.play()