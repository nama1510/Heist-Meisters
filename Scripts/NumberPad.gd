extends Popup

export var combination = []
var guess = []


onready var display = $VSplitContainer/DisplayContainer/Display2
onready var light = $VSplitContainer/ButtonContainer/ButtonGrid/light
onready var audio_player = $AudioStreamPlayer

signal combination_correct

func _ready():
	reset_lock()
	connect_buttons()

func connect_buttons():
	for child in $VSplitContainer/ButtonContainer/ButtonGrid.get_children():
		if child is Button:
			child.connect("pressed", self, "_on_Button_pressed", [child.text])


func _on_Button_pressed(button_text):
	if button_text == "OK":
		check_guess()
	else:
		enter(int(button_text))
		


func check_guess():
	if guess == combination:
		play_audio(Global.combination_correctSFX)
		light.texture = load(Global.green_light)
		$Timer.start()
	else:
		reset_lock()


func enter(button):
	
	play_audio(Global.button_pushedSFX)
	
	guess.append(button)
	update_display()
	if guess.size() == combination.size():
		check_guess()
		

func update_display():
	display.bbcode_text = "[center]" + PoolStringArray(guess).join("") + "[/center]"


func reset_lock():
	light.texture = load(Global.red_light)
	guess.clear()
	display.clear()
	pass


func play_audio(path):
	audio_player.stream = load(path)
	audio_player.play()

func _on_Timer_timeout():
	emit_signal("combination_correct")
	reset_lock()
	hide()
