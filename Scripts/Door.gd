extends Area2D

var can_click = false


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass


func _on_Door_body_entered(body):
	if body != Global.Player && $AnimationPlayer.is_playing() == false:
		open()
	else:
		can_click = true


func _on_Door_body_exited(body):
	if body == Global.Player:
		can_click = false


func open():
	$AnimationPlayer.play("open")


func _input_event(viewport, event, shape_idx):

	if Input.is_mouse_button_pressed(BUTTON_LEFT) && can_click:
		open()
